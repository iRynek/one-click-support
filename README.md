About:
======
Create VNC remote desktop tech help service like Google Chrome Desktop or TeamViever that pass throught firewall 
and NAT (on client and server) for free.

This helper script will run VNC through reverse VNC connection that is passed throught SSH remote server.

How?:
=====
Using reverse VNC connection and SSH remote port forwarding.

Requirements:
=============
* Remote server with open port and SSH access with remote port forwarding enabled. See 
  [GatewayPorts](https://www.freebsd.org/cgi/man.cgi?sshd_config(5)) setting in Your ssh server config.

Tech support machine:

* Windows:
    * [TightVNC](http://www.tightvnc.com/) or when using [choco](https://chocolatey.org/) `choco install tightvnc`
    * [ssh](https://github.com/PowerShell/Win32-OpenSSH/releases) or when using [choco](https://chocolatey.org/)
    `choco install win32-ssh`
    * `one-click-provide-support.bat` on his desktop
* Ubuntu:
    * xvnc4viewer - `sudo apt-get insall xvnc4viewer`
    * `one-click-provide-support.sh` on his desktop

Support receiver machine:

* Windows:
    * [TightVNC](http://www.tightvnc.com/) or when using [choco](https://chocolatey.org/) `choco install tightvnc`
    * `one-click-get-support.bat` on his desktop
* Ubuntu:
    * xvnc4viewer - `sudo apt-get insall xvnc4viewer`
    * `one-click-get-support.sh` on his desktop

Usage:
======

1. Prepare SSH login on remote server and allow port redirection on that login (on ubuntu enabled by default).
2. Open ports that You want to use as gateway (9000-9010), as many as many concurent connections You want to support.
3. Checkout project and modify SSH connection variables + port the client will use (eg. 9000-9010). Each person
   that provide remote help shouch have assigned one port.
4. Ask user that need support to install TightVNC.
6. Run `one-click-provide-help.bat` on Windows `one-click-provide-help.sh` on Ubuntu.
5. Ask user to run `one-click-help.bat` on Windows or `one-click-help.sh` on Ubuntu and provide user host and port, 
   You can change defaults in script.
