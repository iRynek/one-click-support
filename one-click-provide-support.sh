#!/usr/bin/env bash
REMOTE_PORT=9000
LOCAL_PORT=5900

SSH_HOST='vnc.irynek.pl'
SSH_USER='root'
SSH_PORT=22

echo "Configuring SSH tunell..."
(
    ssh -N -p${SSH_PORT} ${SSH_USER}@${SSH_HOST} -R ${REMOTE_PORT}:localhost:${LOCAL_PORT}
) &
echo "Setting up VNC reverse connection..."
echo ""
echo "[Press Ctrl+C to stop...]"

vncviewer -listen ${LOCAL_PORT}


